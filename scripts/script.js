const gridSize = 8;
const totalMines = Math.floor((gridSize * gridSize) / 6);

let flagsCount = 0;
let openedCells = 0;
let isGameOver = false;

const gridContainer = document.getElementById("grid-container");
const flagsCountElement = document.getElementById("flags-count");
const totalMinesElement = document.getElementById("total-mines");
const restartButton = document.getElementById("restart-button");

function initializeGame() {
  flagsCount = 0;
  openedCells = 0;
  isGameOver = false;
  gridContainer.innerHTML = "";
  flagsCountElement.textContent = flagsCount;
  totalMinesElement.textContent = totalMines;

  const minesArray = Array(totalMines).fill("mine");
  const emptyArray = Array(gridSize * gridSize - totalMines).fill("empty");
  const gameArray = emptyArray.concat(minesArray);
  const shuffledArray = gameArray.sort(() => Math.random() - 0.5);

  for (let i = 0; i < gridSize * gridSize; i++) {
    const cell = document.createElement("div");
    cell.classList.add("cell", shuffledArray[i]);
    cell.setAttribute("data-index", i);
    gridContainer.appendChild(cell);

    cell.addEventListener("click", cellClick);
    cell.addEventListener("contextmenu", flagCell);
  }
}

function cellClick(event) {
  if (isGameOver) return;

  const cell = event.target;
  if (cell.classList.contains("clicked") || cell.classList.contains("flagged"))
    return;

  if (cell.classList.contains("mine")) {
    gameOver(cell);
  } else {
    const cellIndex = parseInt(cell.getAttribute("data-index"));
    const minesAround = countMinesAround(cellIndex);
    cell.classList.add("clicked");
    openedCells++;

    if (minesAround > 0) {
      cell.textContent = minesAround;
    } else {
      openEmptyCells(cellIndex);
    }

    if (openedCells === gridSize * gridSize - totalMines) {
      gameWon();
    }
  }
}

function flagCell(event) {
  event.preventDefault();
  if (isGameOver) return;

  const cell = event.target;
  if (cell.classList.contains("clicked")) return;

  if (cell.classList.contains("flagged")) {
    cell.classList.remove("flagged");
    flagsCount--;
  } else {
    cell.classList.add("flagged");
    flagsCount++;
  }

  flagsCountElement.textContent = flagsCount;
}

function countMinesAround(cellIndex) {
  const cellRow = Math.floor(cellIndex / gridSize);
  const cellCol = cellIndex % gridSize;
  let minesCount = 0;

  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      const newRow = cellRow + i;
      const newCol = cellCol + j;
      if (
        newRow >= 0 &&
        newRow < gridSize &&
        newCol >= 0 &&
        newCol < gridSize
      ) {
        const newIndex = newRow * gridSize + newCol;
        const neighborCell = gridContainer.children[newIndex];
        if (neighborCell && neighborCell.classList.contains("mine")) {
          minesCount++;
        }
      }
    }
  }

  return minesCount;
}

function openEmptyCells(cellIndex) {
  const cellRow = Math.floor(cellIndex / gridSize);
  const cellCol = cellIndex % gridSize;

  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      const newRow = cellRow + i;
      const newCol = cellCol + j;
      if (
        newRow >= 0 &&
        newRow < gridSize &&
        newCol >= 0 &&
        newCol < gridSize
      ) {
        const newIndex = newRow * gridSize + newCol;
        const neighborCell = gridContainer.children[newIndex];
        if (neighborCell && !neighborCell.classList.contains("clicked")) {
          neighborCell.click();
        }
      }
    }
  }
}

function gameOver(clickedMine) {
  isGameOver = true;
  gridContainer
    .querySelectorAll(".mine")
    .forEach((mine) => mine.classList.add("clicked"));
  clickedMine.classList.add("exploded");
  setTimeout(() => {
    alert("Програш! Спробуйте ще раз.");
    initializeGame();
  }, 500);
}

function gameWon() {
  isGameOver = true;
  setTimeout(() => {
    alert("Ви виграли! Вітаємо!");
    initializeGame();
  }, 200);
}

initializeGame();

restartButton.addEventListener("click", initializeGame);